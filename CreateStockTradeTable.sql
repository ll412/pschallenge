CREATE DATABASE [sqlchallenge1.DB]
GO

USE [sqlchallenge1.DB]
GO


/****** Object:  Table [dbo].[StockTrade]    Script Date: 9/18/2018 10:30:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[StockTrade](
	[Ticker] [varchar](10) NULL,
	[Trade_Time] [varchar](20) NULL,
	[OpenPrice] [numeric](10, 6) NULL,
	[High] [numeric](10, 6) NULL,
	[Low] [numeric](10, 6) NULL,
	[ClosePrice] [numeric](10, 6) NULL,
	[Volume] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


