#SQL Challenge 1

##The Table Creation

You will find the table creation code;

* [The table Create Statement] (CreateStockTradeTable.sql)


## The SQL code

You will find this in the following file;

* [The Stored Procedure] (usp_CalculateVWAP1.sql)

>This procedure will return the Volume Weighted Average Price considering the ticks in a specified 5 hour interval on any specific day.

## The command to run the procedure:
---

EXEC usp_CalculateVWAP1 '201010291520'

*Note: '201010291520' is a date with date format yyyymmddmm (First mm is Month, Second mm is minute)

----
## Assumptions
* Volume Weighted Average Price: use close price as traded price and use the total traded value divide by total volume: 
SUM(Volume*ClosePrice)/SUM(Volume)

* 5 hour Interval: if the user quoting hour exceed the market close time, close time will be used. For example, 14:30 is the 5 hour interval of 09:00; 16:55 is the time interval of 12:00, if 16:55 is the market close time of the day quoted.

----
## Output
* Volume Weighted Average Price (VWAP)

* Date: dd/mm/yyyy

* Interval: Start(HH:MM) - End(HH:MM)

----
## SP Breakdown
* Split the parameter (user input) into date, hour and minutes and use variables to store these date parts. The reason to store them into variables is that they are used more than once.

* Use IF statement to check the @hour value, if @hour with 5 hour interval exceed market close time of the quoted date, then use the market close time.

* Calculate VMAP by using the data between quoted date and hour interval.

* Convert user input yyyymmdd into the required date format: dd/mm/yyyy.

 