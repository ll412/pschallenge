USE [sqlchallenge1.DB]
GO

--DROP PROCEDURE usp_CalculateVWAP 
--GO

CREATE PROCEDURE usp_CalculateVWAP 
	@date varchar(20)
		
AS

DECLARE @datePart AS VARCHAR(10)
	,@Hour AS VARCHAR(4)
	,@Minute AS VARCHAR(4)
	,@intervalEnd AS VARCHAR(10)

SET @datePart = SUBSTRING(@date, 1, 8)
SET @Hour = SUBSTRING(@date, 9, 2) + 5
SET @Minute = SUBSTRING(@date, 11, 2)


IF @Hour > 16
	SET @intervalEnd = STUFF(SUBSTRING((
					SELECT MAX(Trade_Time)
					FROM StockTrade
					WHERE Trade_Time LIKE @datePart + '%'
					), 9, 12), 3, 0, ':')
ELSE
	SET @intervalEnd = @Hour + ':' + @Minute;


SELECT SUM(Volume * ClosePrice) / SUM(Volume) AS VWAP
	,Convert(VARCHAR, CONVERT(DATETIME, SUBSTRING(@date, 1, 8)), 103) AS [Date]
	,'Start (' + STUFF(SUBSTRING(@date, 9, 4), 3, 0, ':') + ') - End (' + @intervalEnd + ')' AS Interval
FROM StockTrade
WHERE Trade_Time BETWEEN @date
		AND (@datePart + @Hour + @Minute);



