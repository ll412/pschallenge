--SQL Challenge 2
CREATE PROCEDURE usp_MaxTradingRange
AS
SELECT TOP 3 TradeDate
	,MAX(ABS([Open] - [Close])) AS [Range]
INTO #rangeTemp
FROM TradeDetails
GROUP BY TradeDate
ORDER BY [Range] DESC;

SELECT td.TradeDate
	,MAX(High) AS MaxHigh
INTO #maxprice
FROM TradeDetails td
JOIN #rangeTemp tmp ON td.TradeDate = tmp.TradeDate
GROUP BY td.TradeDate;

SELECT td.tradeDate
	,td.TradeTime
	,m.MaxHigh
INTO #MaxPriceTime
FROM TradeDetails td
JOIN #maxprice m ON td.TradeDate = m.TradeDate
	AND td.High = m.MaxHigh;

SELECT r.TradeDate
	,r.[Range]
	,m.TradeTime
FROM #rangeTemp r
JOIN #MaxPriceTime m ON r.TradeDate = m.TradeDate
ORDER BY r.TradeDate;


--Drop Temporary Tables
DROP TABLE #rangeTemp;

DROP TABLE #maxprice;

DROP TABLE #MaxPriceTime;
