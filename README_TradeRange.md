#SQL Challenge 2

##The Table Creation

You will find the table creation code;

* [The table Create Statement] (CreateTradeDetails.sql)


## The SQL code

You will find this in the following file;

* [The Stored Procedure] (usp_MaxTradingRange.sql)

## The command to run the procedure:
---

EXEC usp_MaxTradingRange

----
## Assumptions
* Range is the difference between open value and close value


----
## Output
* Date 1 ; Date 2 ; Date 3 in descending order (biggest range first, second biggest range next etc)
* The trading range per date.
* The time during the day that the stock reached its maximum price.

----
## SP Breakdown
There are 3 temporary tables are created within the script:

* #rangeTemp - contains Date and Range of the 3 trades which have the biggest range. 

*Note: Range of each trade is calculated based on the difference between [open] and [close] value.*
  
* #maxprice - contains Date, Highest [High] value of the 3 days. This is done by joining #rangeTemp table with dataset on the date and group by the date. 
* #MaxPriceTime - contains Date,Time, Highest [High] value of the 3 days. This is done by joining #maxprice table with dataset on the date AND the highest [High] value.

Finally, join #rangeTemp and #MaxPriceTime table on date to retrieve the output: Date, Range, Time. And drop the temporary tables.


