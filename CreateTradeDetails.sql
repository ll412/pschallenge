CREATE DATABASE [sqlchallenge1.DB]
GO

USE [sqlchallenge1.DB]
GO

/****** Object:  Table [dbo].[TradeDetails]    Script Date: 9/18/2018 5:17:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TradeDetails](
	[TradeDate] [varchar](10) NULL,
	[TradeTime] [varchar](4) NULL,
	[Open] [numeric](6, 2) NULL,
	[High] [numeric](6, 3) NULL,
	[Low] [numeric](6, 3) NULL,
	[Close] [numeric](6, 4) NULL,
	[Volume] [int] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


